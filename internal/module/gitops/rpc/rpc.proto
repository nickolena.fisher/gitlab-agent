syntax = "proto3";

// If you make any changes make sure you run: make regenerate-proto

package gitlab.agent.gitops.rpc;

option go_package = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v14/internal/module/gitops/rpc";

import "pkg/agentcfg/agentcfg.proto";
import "internal/tool/grpctool/automata/automata.proto";
//import "github.com/envoyproxy/protoc-gen-validate/blob/master/validate/validate.proto";
import "validate/validate.proto";

message ObjectsToSynchronizeRequest {
  // Project to fetch Kubernetes object manifests from.
  // e.g. gitlab-org/cluster-integration/gitlab-agent
  string project_id = 1 [(validate.rules).string.min_bytes = 1];
  // Last processed commit id. Optional.
  // Server will only send objects if the last commit on the branch is
  // a different one. If a connection breaks, this allows to resume
  // the stream without sending the same data again.
  string commit_id = 2;
  // A list of paths inside of the project to scan
  // for .yaml/.yml/.json manifest files.
  repeated agentcfg.PathCF paths = 3 [(validate.rules).repeated.min_items = 1];
}

message ObjectsToSynchronizeResponse {
  // First message of the stream.
  message Header {
    // Commit id of the manifest repository.
    // Can be used to resume connection from where it dropped.
    string commit_id = 1 [(validate.rules).string.min_bytes = 1];
    // Numeric project id of the manifest repository.
    int64 project_id = 2;
  }
  // Subsequent messages of the stream.
  message Object {
    // Source of the YAML e.g. file name.
    // Several subsequent messages may contain the same source string.
    // That means data should be accumulated to form the whole blob of data.
    string source = 1 [(validate.rules).string.min_bytes = 1];

    // YAML object manifest.
    // Might be partial data, see comment for source.
    bytes data = 2;
  }
  // Last message of the stream.
  message Trailer {
  }
  oneof message {

    option (grpctool.automata.first_allowed_field) = 1;
    option (grpctool.automata.first_allowed_field) = -1; // EOF means there is nothing to do
    option (validate.required) = true;

    Header header = 1 [(grpctool.automata.next_allowed_field) = 2, (grpctool.automata.next_allowed_field) = 3];
    Object object = 2 [(grpctool.automata.next_allowed_field) = 2, (grpctool.automata.next_allowed_field) = 3];
    Trailer trailer = 3 [(grpctool.automata.next_allowed_field) = -1];
  }
}

service Gitops {
  // Fetch Kubernetes objects to synchronize with the cluster.
  // Server closes the stream when it's done transmitting the full batch of
  // objects. New request should be made after that to get the next batch.
  rpc GetObjectsToSynchronize (ObjectsToSynchronizeRequest) returns (stream ObjectsToSynchronizeResponse) {
  }
}
